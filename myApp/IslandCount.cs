using System;
using System.Collections.Generic;
namespace myApp
{
    class IslandCount
    {
        // constructor
        public IslandCount()
        {
            int[,] matrix = new int[,] { { 1, 0, 1, 0 }, { 0, 1, 1, 0 } };

            int rows = matrix.GetLength(0);
            int cols = matrix.GetLength(1);
            int count = 0;
            for (int r = 0; r < rows; r++)
            {
                for (int c = 0; c < cols; c++)
                {
                    if (matrix[r, c] == 1)
                    {
                        count++;
                        dfs(matrix, r, c, rows, cols);
                    }
                }
            }
            Console.WriteLine($"Number of islands: {count}");
        }

        // mark island as 2 (visited)
        public void dfs(int[,] matrix, int r, int c, int rows, int cols)
        {

            //out of bounce
            if (r < 0 || r >= rows || c < 0 || c >= cols)
            {
                return;
            }
            if (matrix[r, c] == 2 || matrix[r, c] == 0)
            {
                return;
            }

            // mark as visited,
            matrix[r, c] = 2;

            //explore
            dfs(matrix, r + 1, c, rows, cols);
            dfs(matrix, r - 1, c, rows, cols);
            dfs(matrix, r, c + 1, rows, cols);
            dfs(matrix, r, c - 1, rows, cols);
        }
    }
}