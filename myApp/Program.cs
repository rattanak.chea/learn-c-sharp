﻿using System;
using System.Collections.Generic;
namespace myApp
{
    class Program
    {
        static void Main(string[] args)
        {

            var names = new List<string> { "<name>", "Ana", "Felipe" };
            foreach (string name in names)
            {
                var str = name.ToLower();
                Console.WriteLine($"{str} is palindrome: {isPalindrome(str)}");
            }

            var island = new IslandCount();
        }

        static bool isPalindrome(string str)
        {
            int left = 0;
            int right = str.Length - 1;

            while (left < right)
            {
                if (str[left] != str[right])
                {
                    return false;
                }
                left++;
                right--;
            }
            return true;
        }
    }
}
